cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)

project(timeout CXX)

add_subdirectory(pause)
add_subdirectory(sigwait)
add_subdirectory(sigwaitinfo)
