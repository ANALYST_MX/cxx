#include "main.hxx"
#include <signal.h>

int main(int argc, char *argv[])
{
  int sig;
  sigset_t set;
  sigemptyset(&set);
  sigaddset(&set, SIGUSR2);
  sigprocmask(SIG_SETMASK, &set, nullptr);
  std::cout << "WAIT SIGUSR2" << std::endl;
  sigwait(&set, &sig);
  std::cout << "Caught signal " << sig << std::endl;

  return EXIT_SUCCESS;
}
