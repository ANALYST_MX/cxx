#include "main.hxx"
#include <signal.h>

int main(int argc, char *argv[])
{
  siginfo_t info;
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, SIGUSR2);
  sigprocmask(SIG_SETMASK, &mask, nullptr);

#if __APPLE__
#ifndef __MACH__
  sigwaitinfo(&mask, &info);
  std::cout << "pid = " << info.si_pid << std::endl;
#endif 
#endif
  
  return EXIT_SUCCESS;
}
