#include "main.hxx"
#include <unistd.h>
#include <signal.h>

int main(int argc, char *argv[])
{
  struct sigaction act;
  sigemptyset(&act.sa_mask);
  act.sa_handler = SIG_DFL;
  act.sa_flags = SA_RESTART;
  if (sigaction(SIGUSR2, &act, nullptr))
    {
      perror ("sigaction");
      return EXIT_FAILURE;
    }
  
  std::cout << "start pause..." << std::endl;
  pause();

  return EXIT_SUCCESS;
}
