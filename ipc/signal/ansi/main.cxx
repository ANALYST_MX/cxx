#include "main.hxx"
#include <signal.h>

int main(int argc, char *argv[])
{
  signal(SIGUSR1, signal_callback_handler);
  signal(SIGUSR2, signal_callback_handler);
  
  raise(SIGUSR1);
  while(true) {}
  return EXIT_SUCCESS;
}

void signal_callback_handler(int signum)
{
  if (signum == SIGUSR1)
    {
      std::cout << "Caught signal " << signum << std::endl;
    }
  else if (signum == SIGUSR2)
    {
      std::cout << "Caught signal " << signum << std::endl;
      std::cout << "exit" << std::endl;
      exit(signum);
    }
}
