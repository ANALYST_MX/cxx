#include "main.hxx"
#include <signal.h>

int main(int argc, char *argv[])
{
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, SIGTERM);
  if (sigprocmask(SIG_SETMASK, &mask, nullptr))
    {
      perror ("sigprocmask");
      return EXIT_FAILURE;
    }
  
  struct sigaction act;
  sigemptyset(&act.sa_mask);
  act.sa_handler = signal_callback_handler;
  act.sa_flags = SA_RESTART;
  if (sigaction(SIGUSR2, &act, nullptr))
    {
      perror ("sigaction");
      return EXIT_FAILURE;
    }
  if (sigaction(SIGUSR1, &act, nullptr))
    {
      perror ("sigaction");
      return EXIT_FAILURE;
    }
  
  raise(SIGTERM);
  while(true) {}
  return EXIT_SUCCESS;
}

void signal_callback_handler(int signum)
{
  if (signum == SIGUSR1)
    {
      std::cout << "Caught signal " << signum << std::endl;
    }
  else if (signum == SIGUSR2)
    {
      std::cout << "Caught signal " << signum << std::endl;
      std::cout << "exit" << std::endl;
      exit(signum);
    }
}
