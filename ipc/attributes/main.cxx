#include "main.hxx"

int main(int argc, char *argv[])
{
  std::cout << "process ID(pid) = " << getpid() << std::endl;
  std::cout << "process's parent ID(ppid) = " << getppid() << std::endl;
  std::cout << "real user ID(uid) = " << getuid() << std::endl;
  std::cout << "real group ID(gid) = " << getgid() << std::endl;
  std::cout << "effective user ID(euid) = " << geteuid() << std::endl;
  std::cout << "effective group ID(egid) = " << getegid() << std::endl;
  char path[PROC_PIDPATHINFO_MAXSIZE];
  getExecutablePath(path, sizeof path);
  std::cout << "path = " << path << std::endl;
  int status = chroot("/");
  std::cout << "change root directory: " << (status != -1 ? "ok" : "fail") << std::endl;
  status = chdir(dirname(dirname(path)));
  std::cout << "change current working directory: " << (status != -1 ? "ok" : "fail") << std::endl;
  std::cout << "scheduling priority: " << nice(0) << std::endl;
  std::cout << "change scheduling priority: " << nice(-4) << std::endl;
  
  return EXIT_SUCCESS;
}

bool getExecutablePath(char *path, size_t lenpath)
{
  bool ok = true;
  if (proc_pidpath(getpid(), path, lenpath) < 0)
    {
      return !ok;
    }
  return ok;
}
