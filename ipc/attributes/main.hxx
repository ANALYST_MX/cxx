#pragma once

#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include <libgen.h>
#include <libproc.h>

bool getExecutablePath(char *path, size_t pathlen);
