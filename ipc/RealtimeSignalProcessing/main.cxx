#include "main.hxx"
#include <signal.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
  struct sigaction sa;
  sa.sa_sigaction = rt_sighandler;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART | SA_SIGINFO;

  if (sigaction(SIGUSR1, &sa, nullptr))
    {
      perror ("sigaction");
      return EXIT_FAILURE;
    }
  if (sigaction(SIGUSR2, &sa, nullptr))
    {
      perror ("sigaction");
      return EXIT_FAILURE;
    }

#if __APPLE__
#ifndef __MACH__
  union sigval value;
  value.sival_int = 32;
  sigqueue(getpid(), SIGUSR2, value);
#endif
#endif
  
  while (true) {}

  return EXIT_SUCCESS;
}

void rt_sighandler(int signum, siginfo_t *info, void *context)
{
  ucontext_t *uc = (ucontext_t *)context;
  if (signum == SIGUSR1)
    {
      std::cout << "Caught signal " << signum << std::endl;
      std::cout << "Got signal " << signum
		<< ", faulty address is " << info->si_addr
		<< ", value is " << info->si_value.sival_int
		<< std::endl;
    }
  else if (signum == SIGUSR2)
    {
      std::cout << "Caught signal " << signum << std::endl;
      std::cout << "exit" << std::endl;
      exit(signum);
    }
  
}
