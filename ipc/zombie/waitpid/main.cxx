#include "main.hxx"
#include <unistd.h>
#include <signal.h>

pid_t pid;

int main(int argc, char *argv[])
{
  pid = fork();
  if (pid == 0)
    {
      pause();
    }
  else
    {      
      struct sigaction act;
      sigemptyset(&act.sa_mask);
      act.sa_handler = signal_callback_handler;
      act.sa_flags = SA_RESTART;
      if (sigaction(SIGCHLD, &act, nullptr))
	{
	  perror ("sigaction");
	  return EXIT_FAILURE;
	}
      if (sigaction(SIGUSR2, &act, nullptr))
	{
	  perror ("sigaction");
	  return EXIT_FAILURE;
	}
      
      kill(pid, SIGKILL);
      while (true) {}
    }
  return EXIT_SUCCESS;
}

void signal_callback_handler(int signum)
{
  if (signum == SIGCHLD)
    {
      int status;
      std::cout << "Caught signal " << signum << std::endl;
      waitpid(pid, &status, 0);
      std::cout << "Close child process with pid = " << pid << std::endl;
    }
  else if (signum == SIGUSR2)
    {
      std::cout << "Caught signal " << signum << std::endl;
      std::cout << "exit" << std::endl;
      exit(signum);
    }
  
}
