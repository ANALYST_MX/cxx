#include "main.hxx"
#include <sys/ipc.h>

int main(int argc, char *argv[])
{
  pid_t pid = fork();
  int id = 13;
  if (pid == 0)
    {
      int idIPC = ftok(argv[0], id);
      std::cout << "child: id = " << idIPC << std::endl;
    }
  else
    {
      int idIPC = ftok(argv[0], id);
      std::cout << "parent: id = " << idIPC << std::endl;
    }
  return EXIT_SUCCESS;
}
