#include "main.hxx"
#include <sys/sem.h>

#define NS 1

#if !defined(_SEM_SEMUN_UNDEFINED)
// definition in <sys/sem.h>
#else
union semun {                 // We define:
  int val;                    // value  for SETVAL
  struct semid_ds *buf;       // buffer for IPC_STAT, IPC_SET
  unsigned short int *array;  // array  for GETALL, SETALL
};
#endif

int main(int argc, char *argv[])
{
  int id = getpid();
  key_t key = ftok(argv[0], id); /* key to pass to semget() */
  int nsems = NS; /* nsems to pass to semget() */
  int semflg = IPC_CREAT | 0666; /* semflg to pass to semget() */
  int semid; /* semid of semaphore set */
  struct sembuf *sops = (struct sembuf *) std::malloc( 2* sizeof(struct sembuf)); /* ptr to operations to perform */
  if ((semid = semget(key, nsems, 0666 | IPC_CREAT)) == -1)
    {
      std::cerr << "error: semget: " << std::strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
  else
    {
      std::cout << "semget succeeded: semid = " << semid << std::endl;
    }

  /* get the semaphore status */
  struct semid_ds sem_buf;
  union semun arg;
  arg.buf = &sem_buf;
  if (semctl(semid, 0, IPC_STAT, arg) == -1)
    {
      std::cerr << "error: semctl[IPC_STAT]: " << std::strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
  std::cout << "Created " << ctime(&sem_buf.sem_ctime);
  std::cout << std::oct;
  std::cout << "Old permissions were " << sem_buf.sem_perm.mode << std::endl;
  std::cout << std::dec;

  /* set the semaphore value */
  arg.val = 0;
  if (semctl(semid, 0, SETVAL, arg) == -1)
    {
      std::cerr << "error: semctl[SETVAL]: " << std::strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }

  /* get the semaphore value */
  int sem_value;
  if ((sem_value = semctl(semid, 0, GETVAL, 0)) == -1)
    {
      std::cerr << "error: semctl[GETVAL]: " << std::strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
  std::cout << "Semaphore has value of " << sem_value << std::endl;

  std::cout << std::endl; 
  /* get child process */
  int pid;
  if ((pid = fork()) == -1)
    {
      std::cerr << "error: fork: " << std::strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
  
  if (pid == 0)
    { /* child */
      int i{ 0 }, j{ 0 };

      while (i < 3)
	{ /* allow for 3 semaphore sets */
	  size_t nsops = 2; /* number of operations to do */

	  /* wait for semaphore to reach zero */
	  sops[0].sem_num = 0; /* We only use one track */
	  sops[0].sem_op = 0; /* wait for semaphore flag to become zero */
	  sops[0].sem_flg = SEM_UNDO; /* take off semaphore asynchronous  */

	  sops[1].sem_num = 0;
	  sops[1].sem_op = 1; /* increment semaphore -- take control of track */
	  sops[1].sem_flg = SEM_UNDO | IPC_NOWAIT; /* take off semaphore */

	  /* Recap the call to be made. */
	  std::cout << "semop: Child Calling semop(" << semid << ", &sops, " << nsops << ") with:" << std::endl;
	  for (j = 0; j != nsops; ++j)
	    {
	      std::cout << "\tsops[" << j << "].sem_num = " << sops[j].sem_num;
	      std::cout << ", sem_op = " << sops[j].sem_op;
	      std::cout << std::oct;
	      std::cout << ", sem_flg = " << sops[j].sem_flg << std::endl;
	      std::cout << std::dec;
	    }

	  /* Make the semop() call and report the results. */
	  if ((j = semop(semid, sops, nsops)) == -1)
	    {
	      std::cerr << "error: semop: " << std::strerror(errno) << std::endl;
	    }
	  else
	    {
	      std::cout << "\tsemop: semop returned " << j << std::endl;
	      std::cout << "Child Process Taking Control of Track: " << i + 1 << "/3 times" << std::endl;
	      sleep(5); /* DO Nothing for 5 seconds */

	      nsops = 1;

	      /* wait for semaphore to reach zero */
	      sops[0].sem_num = 0;
	      sops[0].sem_op = -1; /* Give UP Control of track */
	      sops[0].sem_flg = SEM_UNDO | IPC_NOWAIT; /* take off semaphore, asynchronous  */

	      if ((j = semop(semid, sops, nsops)) == -1)
		{
		  std::cerr << "error: semop: " << std::strerror(errno) << std::endl;
		}
	      else
		{
		  std::cout << "Child Process Giving up Control of Track: " << i + 1 << "/3 times" << std::endl;
		}
	      sleep(5); /* halt process to allow parent to catch semaphor change first */
	    }
	  ++i;
	}
    }
  else /* parent */
    { /* pid hold id of child */
      int i{ 0 }, j{ 0 };

      while (i < 3)
	{ /* allow for 3 semaphore sets */
	  size_t nsops = 2; /* number of operations to do */

	  /* wait for semaphore to reach zero */
	  sops[0].sem_num = 0;
	  sops[0].sem_op = 0; /* wait for semaphore flag to become zero */
	  sops[0].sem_flg = SEM_UNDO; /* take off semaphore asynchronous  */

	  sops[1].sem_num = 0;
	  sops[1].sem_op = 1; /* increment semaphore -- take control of track */
	  sops[1].sem_flg = SEM_UNDO | IPC_NOWAIT; /* take off semaphore */

	  /* Recap the call to be made. */
	  std::cout << "semop: Parent Calling semop(" << semid << ", &sops, " << nsops << ") with:" << std::endl;
	  for (j = 0; j != nsops; ++j)
	    {
	      std::cout << "\tsops[" << j << "].sem_num = " << sops[j].sem_num;
	      std::cout << ", sem_op = " << sops[j].sem_op;
	      std::cout << std::oct;
	      std::cout << ", sem_flg = " << sops[j].sem_flg << std::endl;
	      std::cout << std::dec;
	    }

	  /* Make the semop() call and report the results. */
	  if ((j = semop(semid, sops, nsops)) == -1)
	    {
	      std::cerr << "error: semop: " << std::strerror(errno) << std::endl;
	    }
	  else
	    {
	      std::cout << "\tsemop: semop returned " << j << std::endl;
	      std::cout << "Parent Process Taking Control of Track: " << i + 1 << "/3 times" << std::endl;

	      sleep(5); /* Do nothing for 5 seconds */

	      nsops = 1;

	      /* wait for semaphore to reach zero */
	      sops[0].sem_num = 0;
	      sops[0].sem_op = -1; /* Give UP Control of track */
	      sops[0].sem_flg = SEM_UNDO | IPC_NOWAIT; /* take off semaphore, asynchronous  */

	      if ((j = semop(semid, sops, nsops)) == -1)
		{
		  std::cerr << "error: semop: " << std::strerror(errno) << std::endl;
		}
	      else
		{
		  std::cout << "Parent Process Giving up Control of Track: " << i + 1 << "/3 times" << std::endl;
		}
	      sleep(5); /* halt process to allow child to catch semaphor change first */
	    }
	  ++i;
	}

      wait(nullptr);
      std::cout << std::endl;
      std::cout << "All done waiting for child" << std::endl;
      
      if (semctl(semid, 0, IPC_RMID) == -1)
	{
	  std::cerr << "error: semctl[IPC_RMID]: " << std::strerror(errno) << std::endl;
	}
    }
  //arg.val = 1;
  //semctl(semid, 0, SETVAL, arg);

  return EXIT_SUCCESS;
}
