#include "main.hxx"
#include <sys/shm.h>

#define SHM_SIZE 1024

int main(int argc, char *argv[])
{
  int id = getpid();
  key_t key = ftok(argv[0], id);
  int shmid;
  if ((shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666)) == -1)
    {
      perror("shmget");
      exit(EXIT_FAILURE);
    }

  struct shmid_ds shmid_ds;
  shmctl(shmid, IPC_STAT, &shmid_ds);

  std::cout << "Segment identifier: " << shmid << std::endl;
  std::cout << "Size of segment (bytes): " << shmid_ds.shm_segsz << std::endl;
  std::cout << "PID of creator: " << shmid_ds.shm_cpid << std::endl;
  std::cout << "PID of last shmat(2)/shmdt(2): " << shmid_ds.shm_lpid << std::endl;
  std::cout << "Time of last shmat(): " << ctime(&(shmid_ds.shm_atime));
  std::cout << "Time of last shmdt(): " << ctime(&(shmid_ds.shm_dtime));
  std::cout << "Time of last shmctl() change: " << ctime(&(shmid_ds.shm_ctime));
  std::cout << "Number of current attaches: " << shmid_ds.shm_nattch << std::endl;
  std::cout << "Key supplied to shmget(2): " << std::hex << shmid_ds.shm_perm._key << std::endl;
  std::cout << "Effective UID of owner: " << std::dec << shmid_ds.shm_perm.uid << std::endl;
  std::cout << "Effective GID of owner: " << shmid_ds.shm_perm.gid << std::endl;
  std::cout << "Effective UID of creator: " << shmid_ds.shm_perm.cuid << std::endl;
  std::cout << "Effective GID of creator: " << shmid_ds.shm_perm.cgid << std::endl;
  std::cout << "Permissions + SHM_DEST and SHM_LOCKED flags: " << shmid_ds.shm_perm.mode << std::endl;
  std::cout << "Sequence number: " << shmid_ds.shm_perm._seq << std::endl;

  pid_t pid = fork();
  if (0 == pid)
    {
      sleep(1);
      char *shMemSeg;
      if ((shMemSeg = (char *)shmat(shmid, nullptr, SHM_RDONLY | 644)) == (char *)-1)
	{
	  perror("shmat");
	  exit(EXIT_FAILURE);
	}
      std::cout << shMemSeg << std::endl;
      if (shmdt(shMemSeg) < 0)
	{
	  perror("shmdt");
	  exit(EXIT_FAILURE);
	}
    }
  else
    {
      char *shMemSeg;
      if ((shMemSeg = (char *)shmat(shmid, nullptr, 644)) == (char *)-1)
	{
	  perror("shmat");
	  exit(EXIT_FAILURE);
	}
      bzero(shMemSeg, SHM_SIZE);
      strcpy(shMemSeg, "Hello, World!");
      if (shmdt(shMemSeg) < 0)
	{
	  perror("shmdt");
	  exit(EXIT_FAILURE);
	}
      
      wait(nullptr);
      shmctl(shmid, IPC_RMID, nullptr);
    }
  
  return EXIT_SUCCESS;
}
