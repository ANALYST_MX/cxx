#include "main.hxx"
#include <sys/msg.h>

#define MSGSZ 1024

typedef struct msgbuf
{
  long mtype;
  char mesg[MSGSZ];
} message_buf;

int main(int argc, char *argv[])
{
  int id = 13;
  key_t key = ftok(argv[0], id);
  int msgid = msgget(key, IPC_CREAT | 0664);
  if (msgid == -1)
    {
      perror("msgget failed");
      exit(EXIT_FAILURE);
    }
  std::cout << "KEY is " << std::hex << key << std::endl;
  std::cout << "Message queue created with id " << std::dec << msgid << std::endl;
  struct msqid_ds msgq_status;
  if (msgctl(msgid, IPC_STAT, &msgq_status) == -1)
    {
      perror("msgctl failed");
      exit(EXIT_FAILURE);
    }
  std::cout << "Real user id of the queue creator: " << msgq_status.msg_perm.cuid << std::endl;
  std::cout << "Real group id of the queue creator: " << msgq_status.msg_perm.cgid << std::endl;

  std::cout << "Effective user id of the queue creator: " << msgq_status.msg_perm.uid << std::endl;
  std::cout << "Effective group id of the queue creator: " << msgq_status.msg_perm.gid << std::endl;
  std::cout << "Permissions: " << msgq_status.msg_perm.mode << std::endl;
  std::cout << "Message queue id: " << msgid << std::endl;
  std::cout << msgq_status.msg_qnum << " message(s) on queue" << std::endl;
  std::cout << "Last message sent by process " << msgq_status.msg_lspid << " at " << ctime(&(msgq_status.msg_stime));
  std::cout << "Last message received by process " << msgq_status.msg_lrpid << " at " << ctime(&(msgq_status.msg_rtime));
  std::cout << "Current number of bytes on queue: " << msgq_status.msg_cbytes << std::endl;
  std::cout << "Maximum number of bytes allowed on the queue: " << msgq_status.msg_qbytes << std::endl;
  
  std::cout << "Modify permissions" << std::endl;
  std::cout << "Current permissions: " << msgq_status.msg_perm.mode << std::endl;
  msgq_status.msg_perm.mode = 0644;
  /*
   * msqid_ds.msg_perm.uid, msqid_ds.msg_perm.gid,
   * msqid_ds.msg_perm.mode msg_qbytes
   */
  if (msgctl(msgid, IPC_SET, &msgq_status) == -1)
    {
      perror("msgctl failed");
      exit(EXIT_FAILURE);
    }
  if (msgctl(msgid, IPC_STAT, &msgq_status) == -1)
    {
      perror("msgctl failed");
      exit(EXIT_FAILURE);
    }
  std::cout << "New permissions: " << msgq_status.msg_perm.mode << std::endl;

  pid_t pid = fork();
  if (0 == pid)
    {
      message_buf rbuf;
      if (msgrcv(msgid, &rbuf, MSGSZ, 1, 0) < 0)
	{
	  perror("msgrcv");
	  exit(EXIT_FAILURE);
	}
      std::cout << "Message: \"" << rbuf.mesg  << "\" receive" << std::endl; 
    }
  else
    {
      message_buf sbuf;
      sbuf.mtype = 1;
      strcpy(sbuf.mesg, "I am in the queue?");
      size_t buf_length = strlen(sbuf.mesg) + 1;
      if (msgsnd(msgid, &sbuf, buf_length, IPC_NOWAIT) == -1)
	{
	  perror("msgsnd");
	}
      else
	{
	  std::cout << "Message: \"" << sbuf.mesg << "\" sent" << std::endl;
	}
      
      wait(nullptr);
      std::cout << "Remove the message queue" << std::endl;
      if (msgctl(msgid, IPC_RMID, nullptr) == -1)
	{
	  perror("msgctl failed");
	  exit(EXIT_FAILURE);
	}
    }
  
  return EXIT_SUCCESS;
}
