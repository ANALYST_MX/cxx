#include <iostream>
#include <cstdlib>

int main(int argc, char *argv[])
{
  std::cout << "-*- start exec -*-" << std::endl;
  for (size_t i = 0; i < argc; ++i)
    {
      std::cout << i << ": " << argv[i] << std::endl;
    }
  std::cout << "-*- end exec -*-" << std::endl;
  
  return EXIT_SUCCESS;
}
