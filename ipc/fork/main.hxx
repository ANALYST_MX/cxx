#pragma once

#include <cstdlib>
#include <iostream>
#include <string>
#include <libproc.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

bool getExecutablePath(char *path, size_t pathlen);
