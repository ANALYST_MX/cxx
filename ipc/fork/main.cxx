#include "main.hxx"
#include <unistd.h>
#include <sys/wait.h>
#include <libgen.h> // dirname

int main(int argc, char *argv[])
{
  pid_t pid = fork();
  std::string info;
  if (pid == 0)
    {
      info = "child: ";
      std::cout << info << "pid = " << getpid() << std::endl;
      std::cout << info << "parent pid = " << getppid() << std::endl;
    }
  else
    {
      info = "parent: ";
      std::cout << info << "pid = " << getpid() << std::endl;
      std::cout << info << "parent pid = " << getppid() << std::endl;
      wait(nullptr);
      pid = fork();
      if (pid == 0)
	{
	  execl("exec", "Hello", "Portugal!", (char *)0);
	}
      wait(nullptr);
      pid = fork();
      if (pid == 0)
	{
	  const char *params[] = {"Hello=Portugal!", NULL};
	  execle("exece", "Hello", "Portugal!", (char *)0, params);
	}
      wait(nullptr);
      pid = fork();
      if (pid == 0)
	{
	  char env[] = "PATH=";
	  char path[PROC_PIDPATHINFO_MAXSIZE];
	  getExecutablePath(path, sizeof path);
	  strcpy(path, dirname(path));
	  strcat(env, path);
	  putenv(env);
	  execlp("execp", "Hello", "Portugal!", (char *)0);
	}
      wait(nullptr);
    }
  return EXIT_SUCCESS;
}

bool getExecutablePath(char *path, size_t lenpath)
{
  bool ok = true;
  if (proc_pidpath(getpid(), path, lenpath) < 0)
    {
      return !ok;
    }
  return ok;
}
