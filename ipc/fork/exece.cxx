#include <iostream>
#include <cstdlib>

extern char **environ;

int main(int argc, char *argv[])
{
  std::cout << "-*- start exece -*-" << std::endl;
  for (auto env = environ; *env != nullptr; ++env)
    {
      std::cout << *env << std::endl;
    }
  std::cout << "-*- end exece -*-" << std::endl;
  
  return EXIT_SUCCESS;
}
