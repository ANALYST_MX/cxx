#include <iostream>
#include <cstdlib>

int main(int argc, char *argv[])
{
  std::cout << "-*- start execp -*-" << std::endl;
  char* path = getenv("PATH");
  if (path != nullptr)
    {
      std::cout << "PATH: " << path << std::endl;
    }
  std::cout << "-*- end execp -*-" << std::endl;
  
  return EXIT_SUCCESS;
}
