#include "main.hxx"
#include <unistd.h>
#include <sys/syslimits.h>
#include <cstdio>

/*
 *
 * #define  STDIN_FILENO   0
 * #define STDOUT_FILENO   1
 * #define STDERR_FILENO   2
 */
int main(int argc, char *argv[])
{
  std::cout << "PIPE_BUF: " << PIPE_BUF << std::endl;
  FILE *output = popen("wc -l", "w");
  write_data(output, 100);
  pclose(output);

  string_passed("String passed via pipe");

  who_wc();

  return EXIT_SUCCESS;
}

// who ---(stdout)---> [pfd[1] --- pfd[0]] ---(stdin)---> wc -l
void who_wc()
{
  int pfd[2];
  pipe(pfd);
  if (!fork())
    {
      close(STDOUT_FILENO);
      dup2(pfd[STDOUT_FILENO], STDOUT_FILENO);
      close(pfd[STDOUT_FILENO]);
      close(pfd[STDIN_FILENO]);
      execlp("who", "", "-s", nullptr);
    }
  else
    {
      close(STDIN_FILENO);
      dup2(pfd[STDIN_FILENO], STDIN_FILENO);
      close(pfd[STDIN_FILENO]);
      close(pfd[STDOUT_FILENO]);
      execlp("wc", "", "-l", nullptr);
    }
}

void string_passed(const char *str)
{
  int pipedes[2];
  pid_t pid;
  pipe(pipedes);
  std::cout << "pipe " << pipedes[0] << ": the maximum number of bytes: " << fpathconf(pipedes[0], _PC_PIPE_BUF) << std::endl;
  std::cout << "pipe " << pipedes[1] << ": the maximum number of bytes: " << fpathconf(pipedes[1], _PC_PIPE_BUF) << std::endl;
  if (fork())
    {
      close(pipedes[0]);
      write(pipedes[1], (void *)str, strlen(str) + 1);
      close(pipedes[1]);
    }
  else
    {
      char buf[fpathconf(pipedes[0], _PC_PIPE_BUF)];
      int len;
      close(pipedes[1]);
      while ((len = read(pipedes[0], buf, sizeof buf)) != 0)
	{
	  std::cout << buf << std::endl;
	}
      close(pipedes[0]);
      exit(EXIT_SUCCESS);
    }
}

void write_data(FILE *stream, int l)
{
  for (auto i = 0; i != l; ++i)
    {
      fprintf(stream, "%d\n", i);
    }
}
