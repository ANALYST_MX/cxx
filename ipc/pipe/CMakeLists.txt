cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)

project(pipe CXX)

add_subdirectory(named)
add_subdirectory(anonymous)
add_subdirectory(fd_passing)
