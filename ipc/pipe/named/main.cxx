#include "main.hxx"
#include <unistd.h>
#include <ctime>
#include <sys/file.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
  char path[] = "/tmp/myTmpFile-XXXXXX";
  auto fd = mkstemp(path);
  setlinebuf(stdout);
  unlink(path);
  if (mkfifo(path, 0600) == -1)
    {
      std::cerr << "mkfifo failed: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
  else
    {
      std::cout << path << " is created" << std::endl;
    } 
  pid_t pid = fork();
  if (0 == pid)
    {
      srand(time(nullptr));
      char buf[10];
      for (size_t i = 0; i != sizeof buf; ++i)
	{
	  buf[i] =(char)('0' + rand() % 10);
	}

      int fd;
      if ((fd = open(path, O_WRONLY)) <= 0)
	{
	  std::cerr << "open failed: " << std::strerror(errno) << std::endl;
	  return EXIT_FAILURE;
	}
      else
	{
	  std::cout << path << " is opened" << std::endl;
	}
      write(fd, buf, sizeof buf);
      std::cout << "Child send: " << buf << std::endl;
      close(fd);
    }
  else
    {
      char buf[512]{ 0 };

      int fd;
      if ((fd = open(path, O_RDONLY)) <= 0)
	{
	  std::cerr << "open failed: " << std::strerror(errno) << std::endl;
	  return EXIT_FAILURE;
	}
      else
	{
	  std::cout << path << " is opened" << std::endl;
	}
      if (read(fd, buf, sizeof buf) <= 0)
	{
	  std::cerr << "read failed: " << std::strerror(errno) << std::endl;
	}
      else
	{
	  std::cout << "Parent receive: " << buf << std::endl;
	}
      close(fd);
      wait(nullptr);
      if (unlink(path) == -1)
	{
	  std::cerr << "Could not remove " << path << std::endl;
	}
    }
  
  return EXIT_SUCCESS;
}
