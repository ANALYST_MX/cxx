#pragma once

#include <cstdlib>
#include <iostream>
#include <cstring>

ssize_t sock_fd_write(int sock, void *buf, ssize_t buflen, int fd);
ssize_t sock_fd_read(int sock, char *msg, ssize_t msg_size, int *fd);

void child(int sock);
void parent(int sock);
