#include "main.hxx"
#include <unistd.h>
#include <sys/socket.h>

// http://keithp.com/blogs/fd-passing/
int main(int argc, char *argv[])
{
  int sv[2];
  int pid;

  if (socketpair(AF_LOCAL, SOCK_STREAM, 0, sv) < 0)
    {
      perror("socketpair");
      exit(EXIT_FAILURE);
    }
  
  switch ((pid = fork()))
    {
    case 0:
      close(sv[0]);
      child(sv[1]);
      break;
    case -1:
      perror("fork");
      exit(1);
    default:
      close(sv[1]);
      parent(sv[0]);
      break;
  }
  
  return EXIT_SUCCESS;
}

ssize_t sock_fd_write(int sock, const char *buf, ssize_t buflen, int fd)
{
  char buff[buflen];
  strcpy(buff, buf);
  struct msghdr msg{ 0 };
  struct iovec iov;
  struct cmsghdr *cmsg;
  union
  {
    char control[CMSG_SPACE(sizeof(int))];
    struct cmsghdr align;
  } cmsgu;

  iov.iov_base = buff;
  iov.iov_len = buflen; // buflen != 0
  
  msg.msg_name = nullptr;
  msg.msg_namelen = 0;
  msg.msg_iov = &iov;
  msg.msg_iovlen = 1; // count buf

  if (-1 != fd)
    {
      msg.msg_control = cmsgu.control;
      msg.msg_controllen = sizeof cmsgu.control;
      
      cmsg = CMSG_FIRSTHDR(&msg);
      cmsg->cmsg_len = CMSG_LEN(sizeof(int));
      cmsg->cmsg_level = SOL_SOCKET;
      cmsg->cmsg_type = SCM_RIGHTS;
      std::cout << "passing fd " << fd << std::endl;
      *((int *) CMSG_DATA(cmsg)) = fd;
      msg.msg_controllen = cmsg->cmsg_len;
    }
  else
    {
      msg.msg_control = nullptr;
      msg.msg_controllen = 0;
      std::cout << "not passing fd " << fd << std::endl;
    }
  ssize_t size = sendmsg(sock, &msg, 0);
  if (size < 0)
    {
      perror("sendmsg");
    }
  
  return size;
}

ssize_t sock_fd_read(int sock, char *buf, ssize_t buflen, int *fd)
{
  ssize_t size;
  if (fd)
    {
      struct msghdr msg;
      struct iovec iov;
      union
      {
	char control[CMSG_SPACE(sizeof(int))];
	struct cmsghdr align;
      } cmsgu;
      struct cmsghdr *cmsg;
      
      iov.iov_base = buf;
      iov.iov_len = buflen;
      
      msg.msg_name = nullptr;
      msg.msg_namelen = 0;
      msg.msg_iov = &iov;
      msg.msg_iovlen = 1;
      msg.msg_control = cmsgu.control;
      msg.msg_controllen = sizeof cmsgu.control;
      
      size = recvmsg(sock, &msg, 0);
      
      if (size < 0)
	{
	  perror("sock_fd_read");
	  exit(EXIT_FAILURE);
	}
      
      cmsg = CMSG_FIRSTHDR(&msg);
      
      if (cmsg && cmsg->cmsg_len == CMSG_LEN(sizeof(int)))
	{
	  if (cmsg->cmsg_level != SOL_SOCKET)
	    {
	      std::cerr << "invalid cmsg_level, " << cmsg->cmsg_level << std::endl;
	      exit(EXIT_FAILURE);
	    }
	  if (cmsg->cmsg_type != SCM_RIGHTS)
	    {
	      std::cerr << "invalid cmsg_type, " << cmsg->cmsg_level << std::endl;
	      exit(EXIT_FAILURE);
	    }
	  
	  *fd = *((int *) CMSG_DATA(cmsg));
	}
      else
	{
	  *fd = -1;
	}
    }
  else
    {
      size = read(sock, buf, buflen);
      if (size < 0)
	{
	  perror("read");
	  exit(EXIT_FAILURE);
	}
    }
  
  return size;
}

void child(int sock)
{
  int fd;
  char buf[16];
  ssize_t size;

  sleep(1);
  while(true)
    {
      size = sock_fd_read(sock, buf, sizeof buf, &fd);
      if (size <= 0)
	{
	  break;
	}
      printf ("read %d\n", size);
      if (-1 != fd)
	{
	  write(fd, "hello, world\n", 13);
	  close(fd);
	}
    }
}

void parent(int sock)
{
  ssize_t size;
  int i;
  int fd;

  fd = 1;
  size = sock_fd_write(sock, "1", 1, 1);
  std::cout << "wrote " << size << std::endl;
}
