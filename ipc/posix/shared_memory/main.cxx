#include "main.hxx"
#include <fcntl.h>
#include <sys/mman.h>

#define SHARED_MEMORY_OBJECT_NAME "my_shared_memory"
#define SHARED_MEMORY_OBJECT_SIZE 50
#define SHM_CREATE 1
#define SHM_PRINT  3
#define SHM_CLOSE  4

int main(int argc, char *argv[])
{
  int shm, len, cmd, mode = 0;
  char *addr;

  if (argc < 2 )
    {
      usage(argv[0]);
      return EXIT_FAILURE;
    }

  if ((!strcmp(argv[1], "create") || !strcmp(argv[1], "write")) && (argc == 3))
    {
      len = strlen(argv[2]);
      len = (len <= SHARED_MEMORY_OBJECT_SIZE) ? len : SHARED_MEMORY_OBJECT_SIZE;
      mode = O_CREAT;
      cmd = SHM_CREATE;
    }
  else if (!strcmp(argv[1], "print"))
    {
      cmd = SHM_PRINT;
    }
  else if (!strcmp(argv[1], "unlink"))
    {
      cmd = SHM_CLOSE;
    }
  else
    {
      usage(argv[0]);
      return EXIT_FAILURE;
    }

  if ((shm = shm_open(SHARED_MEMORY_OBJECT_NAME, mode | O_RDWR, S_IRWXU | S_IRWXG)) == -1)
    {
      std::cerr << "shm_open failed with errno " << errno << "<" << std::strerror(errno) << ">" << std::endl;
      return EXIT_FAILURE;
    }

  if (cmd == SHM_CREATE)
    {
      if (ftruncate(shm, SHARED_MEMORY_OBJECT_SIZE + 1) == -1)
	{
	  std::cerr << "ftruncate failed with errno " << errno << "<" << std::strerror(errno) << ">" << std::endl;
	  close(shm);
	  return EXIT_FAILURE;
	}
    }

  addr = (char *)mmap(nullptr, SHARED_MEMORY_OBJECT_SIZE + 1, PROT_WRITE | PROT_READ, MAP_SHARED, shm, 0);
  if (MAP_FAILED == addr)
    {
      std::cerr << "mmap failed with errno " << errno << "<" << std::strerror(errno) << ">" << std::endl;
      close(shm);
      return EXIT_FAILURE;
    }

  switch (cmd)
    {
    case SHM_CREATE:
      memcpy(addr, argv[2], len);
      addr[len] = '\0';
      std::cout << "Shared memory filled in. You may run '" << argv[0] << " print' to see value." << std::endl;
      break;
    case SHM_PRINT:
      std::cout << "Got from shared memory: " << addr << std::endl;
      break;
    }

  munmap(addr, SHARED_MEMORY_OBJECT_SIZE + 1);
  close(shm);
  
  if (cmd == SHM_CLOSE)
    {
      if (shm_unlink(SHARED_MEMORY_OBJECT_NAME) == -1)
	{
	  std::cerr << "shm_unlink failed with errno " << errno << "<" << std::strerror(errno) << ">" << std::endl;
	}
    }
  
  return EXIT_SUCCESS;
}

void usage(const char *app)
{
  std::cout << "Usage: " << app << " <create|write|print|unlink> ['text']" << std::endl;
}
