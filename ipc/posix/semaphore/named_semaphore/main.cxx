#include "main.hxx"
#include <semaphore.h>

#define SEMAPHORE_NAME "/my_named_semaphore"

int main(int argc, char *argv[])
{
  sem_t *sem;
  int value;

  if ( argc == 2 ) {
    std::cout << "Dropping semaphore..." << std::endl;
    if ((sem = sem_open(SEMAPHORE_NAME, 0)) == SEM_FAILED)
      {
	std::cerr << "[shm_open]:\t" << std::strerror(errno) << std::endl;
	return EXIT_FAILURE;
      }
    if (sem_post(sem) == -1)
      {
	std::cout << "[sem_post]:\t" << std::strerror(errno) << std::endl;
	return EXIT_FAILURE;
      }
    std::cout << "Semaphore dropped." << std::endl;
    return EXIT_SUCCESS;
  }

  if ((sem = sem_open(SEMAPHORE_NAME, O_CREAT, 0777, 0)) == SEM_FAILED)
    {
      std::cerr << "[shm_open]:\t" << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }

  sem_getvalue(sem, &value);
  std::cout << "The initial value of the semaphore is " << value << std::endl;

  std::cout << "Semaphore is taken." << std::endl;
  std::cout << "Waiting for it to be dropped." << std::endl;
  if (sem_wait(sem) == -1)
    {
      std::cout << "[sem_wait]:\t" << std::strerror(errno) << std::endl;
    }
  sem_getvalue(sem, &value);
  std::cout << "The value of the semaphore after the wait is " << value << std::endl;
  if (sem_close(sem) == -1)
    {
      std::cout << "[sem_close]:\t" << std::strerror(errno) << std::endl;
    }
  if (sem_unlink(SEMAPHORE_NAME) == -1)
    {
      std::cout << "[sem_unlink]:\t" << std::strerror(errno) << std::endl;
    }
  
  return EXIT_SUCCESS;
}
