cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)

project(binary_semaphore CXX)

add_executable(binary_semaphore main.cxx)
target_compile_features(binary_semaphore PRIVATE cxx_range_for)
