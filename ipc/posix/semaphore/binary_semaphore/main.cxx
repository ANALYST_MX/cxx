#include "main.hxx"
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */
#include <cstdlib>      /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <cstring>      /* String handling */
#include <semaphore.h>  /* Semaphore */

/* global vars */

/*
 * semaphores are declared global so they can be accessed 
 * in main() and in thread routine,
 * here, the semaphore is used as a mutex
 */
sem_t *mutex;

/*
 * shared variable
 */
int counter;

#define SEMAPHORE_NAME "/my_named_semaphore"

int main(int argc, char *argv[])
{
  int i[2];
  pthread_t thread_a;
  pthread_t thread_b;
  i[0] = 0; /* argument to threads */
  i[1] = 1;
#ifdef __APPLE__
  if ((sem_open(SEMAPHORE_NAME, O_CREAT | O_EXCL, 0777, 1)) == SEM_FAILED)
    {
      sem_unlink(SEMAPHORE_NAME);
      if ((mutex = sem_open(SEMAPHORE_NAME, O_CREAT, 0777, 1)) == SEM_FAILED)
	{
	  std::cerr << "error: sem_open: " << std::strerror(errno) << std::endl;
	  return EXIT_FAILURE;
	}
    }
#else
  /* initialize mutex to 1 - binary semaphore */
  /* second param = 0 - semaphore is local */
  if (sem_init(mutex, 0, 1) == -1)
    {
      std::cerr << "error: sem_init: " << std::strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
#endif
  
  /* Note: you can check if thread has been successfully created by checking return value of pthread_create */
  if (pthread_create(&thread_a, nullptr, handler, (void *)&i[0]) != 0)
    {
      std::cerr << "error: pthread_create: "  << std::strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
  if (pthread_create(&thread_b, nullptr, handler, (void *)&i[1]) != 0)
    {
      std::cerr << "error: pthread_create: "  << std::strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
  
  if (pthread_join(thread_a, nullptr) != 0)
    {
      std::cerr << "error: pthread_join: "  << std::strerror(errno) << std::endl;
    }
  if (pthread_join(thread_b, nullptr) != 0)
    {
      std::cerr << "error: pthread_join: "  << std::strerror(errno) << std::endl;
    }
  
  /* destroy semaphore */
#ifdef __APPLE__
  if (sem_close(mutex) == -1)
    {
      std::cout << "[sem_close]:\t" << std::strerror(errno) << std::endl;
    }
  if (sem_unlink(SEMAPHORE_NAME) == -1)
    {
      std::cout << "[sem_unlink]:\t" << std::strerror(errno) << std::endl;
    }
#else
  if (sem_destroy(mutex) == -1)
    {
      std::cerr << "error: sem_destroy: " << std::strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
#endif
  
  return EXIT_SUCCESS;
}

void *handler(void *ptr)
{
  int x;
  x = *((int *) ptr);
  std::cout << "Thread " << x << ": Waiting to enter critical region..." << std::endl;
  /* down semaphore */
  if (sem_wait(mutex) == -1)
    {
      std::cerr << "error: sem_wait: " << std::strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
  /* START CRITICAL REGION */
  std::cout << "Thread " << x << ": Now in critical region..." << std::endl;
  std::cout << "Thread " << x << ": Counter Value: " << counter << std::endl;
  std::cout << "Thread " << x << ": Incrementing Counter..."<< std::endl;
  counter++;
  std::cout << "Thread " << x << ": New Counter Value: " << counter << std::endl;
  std::cout << "Thread " << x << ": Exiting critical region..." << std::endl;
  /* END CRITICAL REGION */
  /* up semaphore */
  if (sem_post(mutex) == -1)
    {
      std::cerr << "error: sem_post: " << std::strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
  
  pthread_exit(EXIT_SUCCESS); /* exit thread */
}
