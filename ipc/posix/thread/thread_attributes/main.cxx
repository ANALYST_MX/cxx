#include "main.hxx"
#include <pthread.h>
#include <unistd.h>

#define THREAD_COUNT 25

int main(int argc, char *argv[])
{
  pthread_t detach_tid;
  pthread_attr_t detach_tattr;

  /* initialize an attribute to the default value */
  pthread_attr_init(&detach_tattr);
  pthread_attr_setdetachstate(&detach_tattr, PTHREAD_CREATE_DETACHED);
  if (pthread_create(&detach_tid, &detach_tattr, detach_entry, nullptr) != 0)
    {
      perror("Creating the detach thread");
      exit(EXIT_FAILURE);
    }

  std::cout << "main thread exit" << std::endl;
  
  /* destroy an attribute */
  pthread_exit(nullptr); // Explicit termination
  pthread_attr_destroy(&detach_tattr);
  return EXIT_SUCCESS;
}

void *detach_entry(void *arg)
{
  for (auto i = 0; i != 3; ++i)
    {
      std::cout << "detach thread: " << i << std::endl;
      sleep(1);
    }
  return nullptr;
}
