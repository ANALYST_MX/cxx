cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)

project(rwlock CXX)

add_executable(rwlock main.cxx)
target_compile_features(rwlock PRIVATE cxx_range_for)
