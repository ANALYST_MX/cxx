#pragma once

#include <cstdlib>
#include <iostream>
#include <cstring>

void *rdlockThread(void *arg);
void *wrlockThread(void *arg);
void checkResults(const char *string, int rc);
