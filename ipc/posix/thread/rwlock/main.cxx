#include "main.hxx"
#include <pthread.h>
#include <unistd.h>

pthread_rwlock_t rwlock;

int main(int argc, char *argv[])
{
  int rc = 0;
  pthread_t thread, thread1;

  std::cout << "Enter Testcase - " << argv[0] << std::endl;

  std::cout << "Main, initialize the read write lock" << std::endl;
  rc = pthread_rwlock_init(&rwlock, nullptr);
  checkResults("pthread_rwlock_init()", rc);

  std::cout << "Main, grab a read lock" << std::endl;
  rc = pthread_rwlock_rdlock(&rwlock);
  checkResults("pthread_rwlock_rdlock()",rc);

  std::cout << "Main, grab the same read lock again" << std::endl;
  rc = pthread_rwlock_rdlock(&rwlock);
  checkResults("pthread_rwlock_rdlock() second", rc);

  std::cout << "Main, create the read lock thread" << std::endl;
  rc = pthread_create(&thread, nullptr, rdlockThread, nullptr);
  checkResults("pthread_create", rc);

  std::cout << "Main - unlock the first read lock" << std::endl;
  rc = pthread_rwlock_unlock(&rwlock);
  checkResults("pthread_rwlock_unlock()", rc);

  std::cout << "Main, create the write lock thread" << std::endl;
  rc = pthread_create(&thread1, nullptr, wrlockThread, nullptr);
  checkResults("pthread_create", rc);

  sleep(5);
  std::cout << "Main - unlock the second read lock" << std::endl;
  rc = pthread_rwlock_unlock(&rwlock);
  checkResults("pthread_rwlock_unlock()", rc);

  std::cout << "Main, wait for the threads" << std::endl;
  rc = pthread_join(thread, nullptr);
  checkResults("pthread_join", rc);

  rc = pthread_join(thread1, nullptr);
  checkResults("pthread_join", rc);

  rc = pthread_rwlock_destroy(&rwlock);
  checkResults("pthread_rwlock_destroy()", rc);

  std::cout << "Main completed" << std::endl;
  
  return EXIT_SUCCESS;
}

void *rdlockThread(void *arg)
{
  int rc;

  std::cout << "Entered thread, getting read lock" << std::endl;
  rc = pthread_rwlock_rdlock(&rwlock);
  checkResults("pthread_rwlock_rdlock()", rc);
  std::cout << "got the rwlock read lock" << std::endl;

  sleep(5);

  std::cout << "unlock the read lock" << std::endl;
  rc = pthread_rwlock_unlock(&rwlock);
  checkResults("pthread_rwlock_unlock()", rc);
  std::cout << "Secondary thread unlocked" << std::endl;
  return nullptr;
}

void *wrlockThread(void *arg)
{
  int rc;

  std::cout << "Entered thread, getting write lock" << std::endl;
  rc = pthread_rwlock_wrlock(&rwlock);
  checkResults("pthread_rwlock_wrlock()", rc);

  std::cout << "Got the rwlock write lock, now unlock" << std::endl;
  rc = pthread_rwlock_unlock(&rwlock);
  checkResults("pthread_rwlock_unlock()", rc);
  std::cout << "Secondary thread unlocked" << std::endl;
  return nullptr;
}

void checkResults(const char *string, int rc)
{
  if (rc != 0)
    {
      std::cerr << "Error on : " << string << ", rc=" << std::strerror(rc) << std::endl;
      exit(EXIT_FAILURE);
    }
}
