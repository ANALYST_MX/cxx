#include "main.hxx"
#include <pthread.h>

#define THREAD_COUNT 25

int main(int argc, char *argv[])
{
  int args[THREAD_COUNT];
  pthread_t threads[THREAD_COUNT];
  
  for (size_t i = 0; i != THREAD_COUNT; ++i)
    {
      args[i] = i;
      if (pthread_create(&threads[i], nullptr, thread_func, &args[i]) != 0)
	{
	  perror("Creating the first thread");
	  exit(EXIT_FAILURE);
	}
    }
  for (size_t i = 0; i != THREAD_COUNT; ++i)
    {
      if (pthread_join(threads[i], nullptr) != 0)
	{
	  perror("Joining the thread");
	  exit(EXIT_FAILURE);
	}
    }

  std::cout << "main thread exit" << std::endl;
    
  return EXIT_SUCCESS;
}

void *thread_func(void *arg)
{
  int i;
  int loc_id = *(int *)arg;
  std::cout << "Thread " << loc_id << " is running" << std::endl;
  return nullptr; //Implicit termination
}
