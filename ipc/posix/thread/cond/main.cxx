#include "main.hxx"
#include <pthread.h>

/* For safe condition variable usage, must use a boolean predicate and  */
/* a mutex with the condition.                                          */
int workToDo = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond2;
pthread_cond_t cond3;

#define NTHREADS 2

int main(int argc, char *argv[])
{
  pthread_condattr_t attr;
  std::cout << "Create the default cond attributes object" << std::endl;
  if (pthread_condattr_init(&attr) != 0)
    {
      std::cerr << "pthread_condattr_init: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
  std::cout << "Create the default conditions in different ways" << std::endl;
  if (pthread_cond_init(&cond2, nullptr) != 0)
    {
      std::cerr << "pthread_cond_init: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
  if (pthread_cond_init(&cond3, &attr) != 0)
    {
      std::cerr << "pthread_cond_init: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
  std::cout << "- At this point, the conditions with default attributes" << std::endl;
  std::cout << "- Can be used from any threads that want to use them" << std::endl;

  std::cout << "Cleanup" << std::endl;
  pthread_condattr_destroy(&attr);
  pthread_cond_destroy(&cond2);
  pthread_cond_destroy(&cond3);

  std::cout << "Create " << NTHREADS << "  threads" << std::endl;
  pthread_t threadid[NTHREADS];
  for (size_t i = 0; i != NTHREADS; ++i)
    {
      if (pthread_create(&threadid[i], nullptr, threadfunc, nullptr) != 0)
	{
	  std::cerr << "pthread_create: " << std::strerror(errno) << std::endl;
	  exit(EXIT_FAILURE);
	}
    }

  sleep(5);
  for (auto i = 0; i != 5; ++i)
    {
      std::cout << "Wake up a worker, work to do..." << std::endl;

      if (pthread_mutex_lock(&mutex) != 0)
	{
	  std::cerr << "pthread_mutex_lock: " << std::strerror(errno) << std::endl;
	  exit(EXIT_FAILURE);
	}

      /* In the real world, all the threads might be busy, and       */
      /* we would add work to a queue instead of simply using a flag */
      /* In that case the boolean predicate might be some boolean    */
      /* statement like: if (the-queue-contains-work)                */
      if (workToDo)
	{
	  std::cout << "Work already present, likely threads are busy" << std::endl;
	}
      workToDo = 1;
      if (pthread_cond_signal(&cond1) != 0)
	{
	  std::cerr << "pthread_cond_signal: " << std::strerror(errno) << std::endl;
	  exit(EXIT_FAILURE);
	}

      if (pthread_mutex_unlock(&mutex) != 0)
	{
	  std::cerr << "pthread_mutex_unlock: " << std::strerror(errno) << std::endl;
	  exit(EXIT_FAILURE);
	}
      sleep(5);  /* Sleep is not a very robust way to serialize threads */
    }
  
  std::cout << "Main completed" << std::endl;
  pthread_cond_destroy(&cond1);
  
  return EXIT_SUCCESS;
}

void *threadfunc(void *parm)
{
  while (1)
  {
      /* Usually worker threads will loop on these operations */
      if (pthread_mutex_lock(&mutex) != 0)
	{
	  std::cerr << "pthread_mutex_lock: " << std::strerror(errno) << std::endl;
	  exit(EXIT_FAILURE);
	}
      
      while (!workToDo)
	{
	  std::cout << "Thread blocked" << std::endl;
	  if (pthread_cond_wait(&cond1, &mutex) != 0)
	    {
	      std::cerr << "pthread_cond_wait: " << std::strerror(errno) << std::endl;
	      exit(EXIT_FAILURE);
	    }
	}
      std::cout << "Thread awake, finish work!" << std::endl;
      
      /* Under protection of the lock, complete or remove the work     */
      /* from whatever worker queue we have. Here it is simply a flag    */
      workToDo = 0;
      
      if (pthread_mutex_unlock(&mutex) != 0)
	{
	  std::cerr << "pthread_mutex_unlock: " << std::strerror(errno) << std::endl;
	  exit(EXIT_FAILURE);
	}
  }
  return nullptr;
}
