#include "main.hxx"
#include <unistd.h>
#include <pthread.h>

int main(int argc, char *argv[])
{
  pthread_t slave_tid;

  if (0 != pthread_create(&slave_tid, nullptr, slave_entry, nullptr))
    {
      std::cerr << "Could not create thread: " << strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
  sleep(10);
  if (0 != pthread_cancel(slave_tid))
    {
      std::cerr << strerror(errno) << std::endl;
    }
  
  pthread_exit(0);
  
  return EXIT_SUCCESS;
}

void *slave_entry(void *arg)
{
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, nullptr);
  pthread_cleanup_push(newline, nullptr);
  std::cout << "slave thread starts" << std::endl;
  while (1)
    {
      std::cout << "." << std::flush;
      pthread_testcancel();
      sleep(1);
    }
  /*
   * Note the pthread_cleanup_pop executes
   * waiting_reader_cleanup.
   */
  pthread_cleanup_pop(1);
  
  return nullptr;
}

void newline(void *arg)
{
  std::cout << std::endl;
  std::cout << "slave thread ends" << std::endl;
}
