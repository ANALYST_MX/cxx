#include "main.hxx"
#include <pthread.h>

pthread_t tid[2];
int counter;
pthread_mutex_t lock;

int main(int argc, char *argv[])
{
  auto i{ 0 };

  if (pthread_mutex_init(&lock, nullptr) != 0)
    {
      std::cerr << "mutex init error: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }

  for (size_t i = 0; i != 2; ++i)
    {
      if (pthread_create(&(tid[i]), nullptr, &doSomeThing, nullptr) != 0)
	{
	  std::cerr << "pthread create error: " << std::strerror(errno) << std::endl;
	}
    }

  if (pthread_join(tid[0], nullptr) != 0)
    {
      std::cerr << "pthread join error: " << std::strerror (errno) << std::endl;
    }
  if (pthread_join(tid[1], nullptr) != 0)
    {
      std::cerr << "pthread join error: " << std::strerror (errno) << std::endl;
    }
  
  if (pthread_mutex_destroy(&lock) != 0)
    {
      std::cerr << "mutex destroy error: " << std::strerror(errno) << std::endl;
    }
  
  return EXIT_SUCCESS;
}

void *doSomeThing(void *arg)
{
  pthread_mutex_lock(&lock);

  counter += 1;
  std::cout << "Job " << counter << " started" << std::endl;

  for (unsigned long i = 0; i != (0xFFFFFFF); ++i);

  std::cout << "Job " << counter << " finished" << std::endl;

  pthread_mutex_unlock(&lock);
  return nullptr;
}
