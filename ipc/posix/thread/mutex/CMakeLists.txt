cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)

project(mutex CXX)

add_executable(mutex main.cxx)
target_compile_features(mutex PRIVATE cxx_range_for)
