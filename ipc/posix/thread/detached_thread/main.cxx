#include "main.hxx"
#include <pthread.h>
#include <unistd.h>

#define THREAD_COUNT 25

int main(int argc, char *argv[])
{
  pthread_t slave_tid;

  if (pthread_create(&slave_tid, nullptr, slave_entry, nullptr) != 0)
    {
      perror("Creating the first thread");
      exit(EXIT_FAILURE);
    }
  
  std::cout << "main thread exit" << std::endl;
  return EXIT_SUCCESS;
}

void *slave_entry(void *arg)
{
  pthread_t self;

  self = pthread_self();
  if (pthread_detach(self) != 0)
    {
      perror("Creating the detach thread");
      return nullptr;
    }

  for (auto i = 0; i != 3; ++i)
    {
      std::cout << "slave thread: " << i << std::endl;
      sleep(1);
    }
  return nullptr;
}
