#include "main.hxx"
#include <pthread.h>

pthread_once_t once = PTHREAD_ONCE_INIT;

int main(int argc, char *argv[])
{
  pthread_t t1, t2;

  pthread_create(&t1, nullptr, slave1_entry, nullptr);
  pthread_create(&t2, nullptr, slave2_entry, nullptr);

  pthread_join(t1, nullptr);
  pthread_join(t2, nullptr);
  
  return EXIT_SUCCESS;
}

void run_once()
{
  std::cout << "\trun_once" << std::endl;
}

void *slave1_entry(void *arg)
{
  std::cout << "T1 enter" << std::endl;
  pthread_once(&once, run_once);
  std::cout << "T1 leave" << std::endl;

  return nullptr;
}

void *slave2_entry(void *arg)
{
  std::cout << "T2 enter" << std::endl;
  pthread_once(&once, run_once);
  std::cout << "T2 leave" << std::endl;

  return nullptr;
}  
