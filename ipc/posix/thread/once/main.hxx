#pragma once

#include <cstdlib>
#include <iostream>

void *slave1_entry(void *arg);
void *slave2_entry(void *arg);
void run_once();
