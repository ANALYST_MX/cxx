#include "main.hxx"
#include <pthread.h>

pthread_t tid[2];
int counter;
#ifdef __APPLE__
pthread_mutex_t lock;
#else
pthread_spinlock_t lock;
#endif

int main(int argc, char *argv[])
{
  auto i{ 0 };

#ifdef __APPLE__
  if (pthread_mutex_init(&lock, nullptr) != 0)
    {
      std::cerr << "mutex init error: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
#else
  if (pthread_spin_init(&lock, PTHREAD_PROCESS_PRIVATE) != 0)
    {
      std::cerr << "spin init error: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
#endif
  
  for (size_t i = 0; i != 2; ++i)
    {
      if (pthread_create(&(tid[i]), nullptr, &doSomeThing, nullptr) != 0)
	{
	  std::cerr << "pthread create error: " << std::strerror(errno) << std::endl;
	}
    }

  if (pthread_join(tid[0], nullptr) != 0)
    {
      std::cerr << "pthread join error: " << std::strerror (errno) << std::endl;
    }
  if (pthread_join(tid[1], nullptr) != 0)
    {
      std::cerr << "pthread join error: " << std::strerror (errno) << std::endl;
    }

#ifdef __APPLE__
  if (pthread_mutex_destroy(&lock) != 0)
    {
      std::cerr << "mutex destroy error: " << std::strerror(errno) << std::endl;
    }
#else
  if (pthread_spin_destroy(&lock) != 0)
    {
      std::cerr << "spin destroy error: " << std::strerror(errno) << std::endl;
    }
#endif
  
  return EXIT_SUCCESS;
}

void *doSomeThing(void *arg)
{
#ifdef __APPLE__
  while (pthread_mutex_trylock(&lock) != 0);
#else
  pthread_spin_lock(&lock);
#endif
  
  counter += 1;
  std::cout << "Job " << counter << " started" << std::endl;

  for (unsigned long i = 0; i != (0xFFFF); ++i);

  std::cout << "Job " << counter << " finished" << std::endl;

#ifdef __APPLE__
  pthread_mutex_unlock(&lock);
#else
  pthread_spin_unlock(&lock);
#endif
  return nullptr;
}
