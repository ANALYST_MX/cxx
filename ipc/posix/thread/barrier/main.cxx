#include "main.hxx"
#include <pthread.h>
#include <array>
#include <algorithm>
#include <iterator>

#define NTHREAD 4
std::array<int, NTHREAD> t = { 10, 20, 30, 40 };
pthread_barrier_t our_barrier;

int main(int argc, char *argv[])
{
  void *(*threadfuncs[NTHREAD])(void*){ thread1, thread2, thread3, thread4 };
  pthread_t threads[NTHREAD];
  pthread_attr_t attr;
  int ret;
  void *res;
  pthread_barrier_init(&our_barrier, nullptr, NTHREAD);
  for (size_t i = 0; i != NTHREAD; ++i)
    {
      if (pthread_create(&threads[i], nullptr, threadfuncs[i], nullptr) != 0)
	{
	  std::cerr << "Unable to create thread[" << i << "]: " << std::strerror(errno) << std::endl;
	}
    }
  std::cout << "Created threads" << std::endl;
  for (size_t i = 0; i != NTHREAD; ++i)
    {
      pthread_join(threads[i], nullptr);
    }

  pthread_barrier_destroy(&our_barrier);
  
  return EXIT_SUCCESS;
}

void *thread1(void *arg)
{
  sleep(2);

  pthread_barrier_wait(&our_barrier);

  std::cout << "thread1: values entered by the threads are ";
  copy(t.cbegin(), t.cend(), std::ostream_iterator<int>(std::cout, ", "));
  std::cout << std::endl;
}


void *thread2(void *arg)
{
  sleep(4);

  pthread_barrier_wait(&our_barrier);

  std::cout << "thread2: values entered by the threads are ";
  std::copy(t.cbegin(), t.cend(), std::ostream_iterator<int>(std::cout, ", "));
  std::cout << std::endl;
}

void *thread3(void *arg)
{
  sleep(6);

  pthread_barrier_wait(&our_barrier);

  std::cout << "thread3: values entered by the threads are ";
  std::copy(t.cbegin(), t.cend(), std::ostream_iterator<int>(std::cout, ", "));
  std::cout << std::endl;
}

void *thread4(void *arg)
{
  sleep(8);

  pthread_barrier_wait(&our_barrier);

  std::cout << "thread4: values entered by the threads are ";
  std::copy(t.cbegin(), t.cend(), std::ostream_iterator<int>(std::cout, ", "));
  std::cout << std::endl;
}

#ifdef __APPLE__
int pthread_barrier_init(pthread_barrier_t *barrier, const pthread_barrierattr_t *attr, unsigned int count)
{
  if (0 == count)
    {
      errno = EINVAL;
      
      return -1;
    }
  if (pthread_mutex_init(&barrier->mutex, 0) < 0)
    {
      return -1;
    }
  if (pthread_cond_init(&barrier->cond, 0) < 0)
    {
      pthread_mutex_destroy(&barrier->mutex);
      
      return -1;
    }
  barrier->tripCount = count;
  barrier->count = 0;

  return 0;
}

int pthread_barrier_destroy(pthread_barrier_t *barrier)
{
  pthread_cond_destroy(&barrier->cond);
  pthread_mutex_destroy(&barrier->mutex);
  
  return 0;
}

int pthread_barrier_wait(pthread_barrier_t *barrier)
{
  pthread_mutex_lock(&barrier->mutex);
  ++(barrier->count);
  if (barrier->count >= barrier->tripCount)
    {
      barrier->count = 0;
      pthread_cond_broadcast(&barrier->cond);
      pthread_mutex_unlock(&barrier->mutex);
      
      return 1;
    }
  else
    {
      pthread_cond_wait(&barrier->cond, &(barrier->mutex));
      pthread_mutex_unlock(&barrier->mutex);
      
      return 0;
    }
}
#endif
