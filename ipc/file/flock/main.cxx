#include "main.hxx"
#include <unistd.h>
#include <ctime>
#include <sys/file.h>

int p = 0;

int main(int argc, char *argv[])
{
  char path[] = "/tmp/myTmpFile-XXXXXX";
  auto fd = mkstemp(path);
  std::cout << path << std::endl;
  pid_t pid = fork();
  if (0 == pid)
    {
      srand(time(nullptr));
      pid_t ppid = getpid();
      for (p = 0; p != 255; ++p)
	{
	  if (getpid() == ppid)
	    {
	      pid = fork();
	    }
	  else
	    {
	      pid = getpid();
	    }
	  if (0 == pid)
	    {
	      if ((rand() % 2))
		{
		  writer(path);
		}
	      else
		{
		  reader(path);
		}
	    }
	  else if (getpid() == ppid)
	    {
	      wait(nullptr);
	    }
	}
    }
  else
    {
      wait(nullptr);
    }
  return EXIT_SUCCESS;
}

void writer(char *path)
{
  char buf[1] = { (char)p };
  auto fd = open(path, O_WRONLY|O_APPEND);
  flock(fd, LOCK_EX);
  write(fd, buf, 1);
  flock(fd, LOCK_UN);
  close(fd);
  ++buf[0];
}

void reader(char *path)
{
  char buf[1];
  auto fd = open(path, O_RDONLY);
  flock(fd, LOCK_SH);
  read(fd, buf, 1);
  flock(fd, LOCK_UN);
  close(fd);
}
