#include "main.hxx"
#include <fcntl.h> // open
#include <unistd.h> //close

int main(int argc, char *argv[])
{
  char path[] = ".lock";
  if (lock(path))
    {
      std::cout << "Lock ok" << std::endl;
      unlock(path);
    }
  else
    {
      std::cout << "Lock fail" << std::endl;
    }
  
  return EXIT_SUCCESS;
}

bool lock(char *filename)
{
  auto fd = open(filename, O_WRONLY | O_CREAT | O_EXCL, 0);
  if (-1 == fd)
    {
      return false;
    }
  close(fd);
  return true;
}

bool unlock(char *filename)
{
  unlink(filename);
  return true;
}
