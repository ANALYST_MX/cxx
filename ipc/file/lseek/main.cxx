#include "main.hxx"
#include <unistd.h>
#include <sys/file.h>
#include <ctime>

int main(int argc, char *argv[])
{
  char filename[] = "/tmp/myTmpFile-XXXXXX";
  auto fd = mkstemp(filename);
  pid_t pid = fork();

  if (0 == pid)
    {
      sleep(1);
      std::cout << "get value: " << lseek(fd, 0, SEEK_CUR) << std::endl;
    }
  else
    {
      srand(time(nullptr));
      int value = rand();
      std::cout << "send value: " << value << std::endl;
      lseek(fd, value, SEEK_SET);
      wait(nullptr);
      unlink(filename);
    }
  return EXIT_SUCCESS;
}
