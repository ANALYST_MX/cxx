#include "main.hxx"
#include <cstring>

#define FL_OVWR 1 /* Overwrite flag. You may change it. */

int main(int argc, char *argv[])
{
  if (argc < 3 || strlen(argv[1]) == 0)
    {
      std::cerr << "error: Too few argements" << std::endl;
      std::cerr << "Usage: " << argv[0] << " <variable> <value>" << std::endl;
      exit(EXIT_FAILURE);
    }
  
  if (setenv(argv[1], argv[2], FL_OVWR) != 0)
    {
      std::cout << "setenv: Cannot set \"" << argv[1] << "\"" << std::endl;
    }
  else
    {
      std::cout << argv[1] << "=" << getenv(argv[1]) << std::endl;
    }
  
  return EXIT_SUCCESS;
}
