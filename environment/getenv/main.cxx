#include "main.hxx"
#include <cstring>

int main(int argc, char *argv[])
{
  if (argc < 2 || strlen(argv[1]) == 0)
    {
      std::cerr << "error: Too few argements" << std::endl;
      std::cerr << "Usage: " << argv[0] << " <variable>" << std::endl;
      exit(EXIT_FAILURE);
    }
  char *val = getenv(argv[1]);
  if (nullptr == val)
    {
      std::cout << "\"" << argv[1] << "\" not found" << std::endl;
    }
  else
    {
      std::cout << "\"" << argv[1] << "=" << val << "\" found" << std::endl;
    }
  
  return EXIT_SUCCESS;
}
