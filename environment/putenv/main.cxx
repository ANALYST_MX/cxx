#include "main.hxx"
#include <cstring>
#include <array>
#include <cerrno> 

#define QUERY_MAX_SIZE 32

int main(int argc, char *argv[])
{
  std::array<char, QUERY_MAX_SIZE> queryStr;
  std::memcpy(queryStr.data(), "FOO=foo_value1", QUERY_MAX_SIZE-1);
  if (-1 == putenv(queryStr.data()))
    {
      std::cerr << std::strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
  print_evar("FOO");
  std::memcpy(queryStr.data(), "FOO=foo_value2", QUERY_MAX_SIZE-1);
  print_evar("FOO");
  std::memcpy(queryStr.data(), "FOO", QUERY_MAX_SIZE-1);
  if (-1 == putenv(queryStr.data()))
    {
      std::cerr << std::strerror(errno) << std::endl;
    }
  print_evar("FOO");

  return EXIT_SUCCESS;
}

void print_evar(const char *var)
{
  char *tmp = getenv(var);
  if (nullptr == tmp)
    {
      std::cout << "\"" << var << "\" is not set" << std::endl;
    }
  else
    {
      std::cout << var << "=" << tmp << std::endl;
    }
}
