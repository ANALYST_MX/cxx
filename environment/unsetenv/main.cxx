#include "main.hxx"

int main(int argc, char *argv[])
{
  char *x;

  /* set environment variable _EDC_ANSI_OPEN_DEFAULT to "Y" */
  setenv("_EDC_ANSI_OPEN_DEFAULT", "Y", 1);
  
  /* set x to the current value of the _EDC_ANSI_OPEN_DEFAULT*/
  x = getenv("_EDC_ANSI_OPEN_DEFAULT");

  std::cout << "_EDC_ANSI_OPEN_DEFAULT = "
	    << (nullptr != x ? x : "undefined")
	    << std::endl;

  /* delete the Environment Variable */
  unsetenv("_EDC_ANSI_OPEN_DEFAULT");
  
  /* set x to the current value of the _EDC_ANSI_OPEN_DEFAULT*/
  x = getenv("_EDC_ANSI_OPEN_DEFAULT");

  std::cout << "_EDC_ANSI_OPEN_DEFAULT = "
	    << (nullptr != x ? x : "undefined")
	    << std::endl;
  
  return EXIT_SUCCESS;
}
