#include "main.hxx"

#include <cstring>
#include <unistd.h>
#include <string>
#include <unordered_set>
#include <algorithm>
#include <iterator>

extern char **environ; /* Environment itself */

int main(int argc, char *argv[])
{
  if (argc < 2 || strlen(argv[1]) == 0)
    {
      std::cerr << "error: Too few argements" << std::endl;
      std::cerr << "Usage: " << argv[0] << " <variable>" << std::endl;
      exit(EXIT_FAILURE);
    }

  std::unordered_set<std::string> result;
  for (size_t i{ 0 }; environ[i] != nullptr; ++i)
    {
      if (strncmp(environ[i], argv[1], strlen(argv[1])) == 0)
	{
	  result.insert(environ[i]);
	}
    }
  
  if (result.empty())
    {
      std::cout << "\"" << argv[1] << "\" not found" << std::endl;
    }
  else
    {
      std::copy(result.cbegin(), result.cend(),
		std::ostream_iterator<std::string>(std::cout, "\n"));
    }

  return EXIT_SUCCESS;
}
