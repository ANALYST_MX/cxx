#include "main.hxx"

int main(int argc, char *argv[])
{
  QCoreApplication app(argc, argv);
  QTextStream out(stdout);
  
  out << "1. Starting the application" << endl; out.flush();
  std::cout << "2. Some normal iostream output before using qDebug" << std::endl;
  qDebug() << "3. Some output with qDebug after the iostream output";

  QTimer::singleShot(1000, &app, SLOT(quit()));
  
  return app.exec();
}
