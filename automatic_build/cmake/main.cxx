#include "main.hxx"

int main(int argc, char *argv[])
{
  shared_library();
  static_library();
  print_hello_world();
  
  return EXIT_SUCCESS;
}

void print_hello_world(void)
{
  std::cout << "Hello, World!" << std::endl;
}
