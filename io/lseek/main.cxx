#include "main.hxx"
#include <fcntl.h>
#include <unistd.h>
#include <cstdio>

static std::array<char, N_COLS> buffer;

int main(int argc, char *argv[])
{
  int a{}, b{}, c{}, i{};
  mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
  int flags = O_WRONLY | O_CREAT | O_TRUNC;
  char ch{};
  auto fd = open(IMG_FN, flags, mode);
  if (-1 == fd)
    {
      std::cerr << "Cannot open file '" << IMG_FN << "': " << strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
        
  init_draw(fd);
  const char *icode[] = { "v 1 1 11", "v 11 7 11", "v 14 5 11", 
			  "v 18 6 11", "v 21 5 10", "v 25 5 10", "v 29 5 6", "v 33 5 6", 
			  "v 29 10 11", "v 33 10 11", "h 11 1 8", "h 5 16 17", 
			  "h 11 22 24", "p 11 5 0", "p 15 6 0", "p 26 11 0", "p 30 7 0", 
			  "p 32 7 0", "p 31 8 0", "p 30 9 0", "p 32 9 0", nullptr }; 

  while (nullptr != icode[i])
    {
      std::sscanf(icode[i], "%c %d %d %d", &ch, &a, &b, &c);
      switch(ch)
	{
	case 'v': draw_vline(fd, a, b, c); break;
	case 'h': draw_hline(fd, a, b, c); break;
	case 'p': draw_point(fd, a, b); break;
	default: abort();    
	}
      ++i;
    }
  
  if (-1 == close(fd))
    {
      std::cerr << "Cannot close file (descriptor=" << fd << ")" << std::endl;
      exit(EXIT_FAILURE);
    }
  
  return EXIT_SUCCESS;
}

void init_draw (int fd)
{       
  ssize_t bytes_written = 0;
  buffer.fill(' ');
  buffer[N_COLS] = '\n';
  while (bytes_written < (N_ROWS * (N_COLS+1)))
    {
      bytes_written += write(fd, buffer.data(), N_COLS + 1);
    }
}

void draw_point (int fd, int x, int y)
{
  auto ch = FG_CHAR;
  lseek(fd, y * (N_COLS + 1) + x, SEEK_SET);
  write(fd, &ch, 1);
}

void draw_hline (int fd, int y, int x1, int x2)
{       
  size_t bytes_write = abs(x2 - x1) + 1;
  std::memset(buffer.data(), FG_CHAR, bytes_write);
  lseek(fd, y * (N_COLS + 1) + std::min(x1, x2), SEEK_SET);
  write(fd, buffer.data(), bytes_write);
}

void draw_vline(int fd, int x, int y1, int y2)
{
  for (auto i = std::min(y1, y2); i <= std::max(y2, y1); ++i)
    {
      draw_point(fd, x, i);
    }
}
