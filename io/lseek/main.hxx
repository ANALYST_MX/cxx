#pragma once

#include <cstdlib>
#include <iostream>
#include <cstring>
#include <algorithm> // max, min
#include <array>

#define N_ROWS  15      /* Image height */
#define N_COLS  40      /* Image width */
#define FG_CHAR 'O'     /* Foreground character */
#define IMG_FN  "image" /* Image filename */

void init_draw(int fd);
void draw_point(int fd, int x, int y);
void draw_hline(int fd, int y, int x1, int x2);
void draw_vline(int fd, int x, int y1, int y2);
