#pragma once

#include <cstdlib>
#include <iostream>
#include <sys/types.h>
#include <dirent.h>

int regfile(struct dirent *ent);
int cmpfile(const void *ent_a, const void *ent_b);
