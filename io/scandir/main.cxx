#include "main.hxx"
#include <cstring>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
  if (argc < 2 || strlen(argv[1]) == 0)
    {
      std::cerr << "error: Too few arguments" << std::endl;
      std::cerr << "Usage: " << argv[0] << " <directory>" << std::endl;
      exit(EXIT_FAILURE);
    }
  struct dirent **namelist;
  int n;
  if ((n = scandir(argv[1], &namelist, regfile, cmpfile)) == -1)
    {
      std::cerr << "scandir error:[" << argv[1] << "] "<< std::strerror(errno) << std::endl;
    }
  else
    {
      std::cout << "Found files: " << n << std::endl;
      while(n--)
	{
	  std::cout << "File: '" << namelist[n]->d_name << "'" << std::endl;
	  free(namelist[n]);
	}
      free(namelist);
    }
  
  return EXIT_SUCCESS;
}

int regfile(struct dirent *ent)
{
  struct stat st;
  bzero(&st, sizeof st);

  if (-1 == stat(ent->d_name, &st))
    {
      std::cerr << "Error stating file '" << ent->d_name << "'" << std::endl;
      return 0;
    }

  if (!S_ISREG(st.st_mode))
    {
      return 0;
    }
  
  return 1;
}

int cmpfile(const void *ent_a, const void *ent_b)
{
  struct stat st_a, st_b;
  bzero(&st_a, sizeof st_a);
  bzero(&st_b, sizeof st_b);
  stat(((const struct dirent *)ent_a)->d_name, &st_a);
  stat(((const struct dirent *)ent_b)->d_name, &st_b);
  return st_a.st_size - st_b.st_size;
}
