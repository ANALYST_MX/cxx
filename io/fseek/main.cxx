#include "main.hxx"
#include <cstring>

#define BIG_SIZE 0x1000000

int main(int argc, char *argv[])
{
  if (argc < 2 && strlen(argv[1]) > 0)
    {
      std::cerr << "error: Too few arguments" << std::endl;
      std::cerr << "Usage: " << argv[0] << " <filename>" << std::endl;
      exit(EXIT_FAILURE);
    }
  
  FILE *f;
  if ((f = fopen(argv[1], "w")) == nullptr) 
    {
      std::cerr << "Can't open " << argv[1] << ", reason: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
  
  if (fwrite(argv[1], 1, strlen(argv[1]), f) < 0)
    {
      std::cerr << "write file " << argv[1] << " fail, errno: " << errno << ", error info: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
  
  if (fseek(f, BIG_SIZE, SEEK_CUR) == -1)
    {
      std::cerr << "Test failed, fseek return fail code. errno=" << errno << "(" << std::strerror(errno) << ")" << std::endl;
      return EXIT_FAILURE;
    }
  
  if (fwrite(argv[1], 1, strlen(argv[1]), f) < 0)
    {
      std::cerr << "write file " << argv[1] << " fail, errno: " << errno << ", error info: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
  
  fclose(f);
  
  return EXIT_SUCCESS;
}
 
