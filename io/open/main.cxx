#include "main.hxx"
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
  int fd;
  mode_t mode = S_IRUSR | S_IWUSR;
  int flags = O_WRONLY | O_CREAT | O_EXCL;
  if (argc < 2 && strlen(argv[1]) > 0)
    {
      std::cerr << "error: Too few arguments" << std::endl;
      std::cerr << "Usage: " << argv[0] << " <filename>" << std::endl;
      exit(EXIT_FAILURE);
    }

  fd = open(argv[1], flags, mode);
  if (-1 == fd)
    {
      std::cerr << "Cannot create and open file '" << argv[1] << "': " << strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
        
  if (-1 == close(fd))
    {
      std::cerr << "Cannot close file (descriptor=" << fd << ")" << std::endl;
      exit(EXIT_FAILURE);
    }
  
  return EXIT_SUCCESS;
}
