#include "main.hxx"
#include <cstdio>
#include <cstring>
#include <unistd.h>
#include <fcntl.h>

#define OFFSET 64

int main(int argc, char *argv[])
{
  int fd; 
  char str[OFFSET];
  std::memset(str, 0x20, OFFSET);
  struct flock fi;
  int off;
  const char *path = "testlocks.txt";

  std::cout << "pid: " << getpid() << std::endl;
  if ((fd = open(path, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR)) == -1)
    {
      std::cerr << "Cannot create and open file '" << path << "': " << strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
  
  fi.l_type = F_WRLCK;
  fi.l_whence = SEEK_SET;
  fi.l_start = 0;
  fi.l_len = OFFSET;
  off = 0;
  while (fcntl(fd, F_SETLK, &fi) == -1)
    {
      fcntl(fd, F_GETLK, &fi);
      std::cerr << "Cannot lock file '" << path << "' (" << fd << "): error [" << std::strerror(errno) << "], (owned by pid " << fi.l_pid << ")" << std::endl;
      off += OFFSET;
      fi.l_start = off;
    }
  
  if (lseek(fd, off, SEEK_SET) == -1)
  {
    std::cerr << "lseek failed: " << std::strerror(errno) << std::endl;
  }

  if (write(fd, str, strlen(str)) == -1)
    {
      std::cerr << "fail to write bytes to child, errno: " << errno << ", errmsg: " << std::strerror(errno) << std::endl;
    }
  
  getchar();
  
  fi.l_type = F_UNLCK;
  if (fcntl(fd, F_SETLK, &fi) == -1)
    {
      std::cerr << "fcntl F_SETFL [" << fd << "] error [" << std::strerror(errno) << "]" << std::endl;
    }
  
  close(fd);
  
  return EXIT_SUCCESS;
}
