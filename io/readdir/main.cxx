#include "main.hxx"
#include <dirent.h>

int main(int argc, char *argv[])
{
  if (argc < 2 || strlen(argv[1]) == 0)
    {
      std::cerr << "error: Too few arguments" << std::endl;
      std::cerr << "Usage: " << argv[0] << " <directory>" << std::endl;
      exit(EXIT_FAILURE);
    }
  
  auto dir = opendir(argv[1]);
  if (nullptr == dir)
    {
      std::cerr << "opendir failed: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
  
  struct dirent *entry;
  while ((entry = readdir(dir)) != nullptr)
    {
      if (entry->d_name[0] != '.')
	{
	  std::cout << entry->d_name << " inode=" << entry->d_ino << std::endl;
	}
    }
  
  closedir(dir);
  
  return EXIT_SUCCESS;
}
