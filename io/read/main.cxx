#include "main.hxx"
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/uio.h>

#define BUF_SIZE 1024

int main(int argc, char *argv[])
{
  int fd;
  int flags = O_RDONLY;
  char buf[BUF_SIZE]{ 0 };
  ssize_t num_bytes;
  if (argc < 2 && strlen(argv[1]) > 0)
    {
      std::cerr << "error: Too few arguments" << std::endl;
      std::cerr << "Usage: " << argv[1] << " <filename>" << std::endl;
      exit(EXIT_FAILURE);
    }

  fd = open(argv[1], flags);
  if (-1 == fd)
    {
      std::cerr << "Cannot open file '" << argv[1] << "': " << strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }

  while ((num_bytes = read(fd, buf, BUF_SIZE - 1)) > 0)
    {
      std::cout << buf << std::endl;
    }

  if (-1 == num_bytes)
    {
      std::cerr << "Read failed with " << strerror(errno) << std::endl;
      exit(EXIT_FAILURE);
    }
  
  if (-1 == close(fd))
    {
      std::cerr << "Cannot close file (descriptor=" << fd << ")" << std::endl;
      exit(EXIT_FAILURE);
    }
  
  return EXIT_SUCCESS;
}
