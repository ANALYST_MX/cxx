cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)

project(io CXX)

add_subdirectory(open)
add_subdirectory(read)
add_subdirectory(lseek)
add_subdirectory(scandir)
add_subdirectory(readdir)
add_subdirectory(fseek)
add_subdirectory(fcntl)
