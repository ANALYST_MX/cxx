#include "main.hxx"
#include <sys/socket.h> // socket
#include <netinet/in.h> // IPPROTO_TCP
#include <strings.h> // bzero
#include <unistd.h> // close
#include <fcntl.h> // fcntl
#include <sys/ioctl.h> // ioctl

int main(int argc, char *arcv[])
{
  server();
  
  return EXIT_SUCCESS;
}

int set_nonblock(int fd)
{
  int flags;
#if defined(O_NONBLOCK)
  if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
    {
      flags = 0;
    }
  return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
  flags = 1;
  return ioctl(fd, FIOBIO, &flags);
#endif
}

void server()
{
  int status = 0;
  /* -*- Domain -*-
   * AF_INET - IPv4
   * AF_INET6 - IPv6
   * AF_INET
   * -*- Type -*-
   * SOCK_STREAM - TCP
   * SOCK_DGRAM - UDP
   * -*- Protocol -*-
   * IPPROTO_TCP
   * IPPROTO_UDP
   */
  int masterSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (masterSocket < 0)
    {
      perror ( "socket" );
      exit ( EXIT_FAILURE );
    }
  
  int optval = 1;
  status = setsockopt(masterSocket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);
  if (-1 == status)
    {
      perror("setsockopt(...,SO_REUSEADDR,...)");
    }

  struct timeval tv;
  tv.tv_sec = 16; // 16 sec timeout
  tv.tv_usec = 0;
  /*
   * SO_RCVTIMEO - recept timeout
   * SO_SNDTIMEO - send timeout
   */
  setsockopt(masterSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof tv);
  /*
   * UNIX - socket:
   * struct sockaddr_un sa;
   * sa.sun_family = AF_UNIX;
   * strcpy(sa.sun_path, "/tmp/a.sock");
   */
  struct sockaddr_in sockAddr;
  bzero(&sockAddr, sizeof sockAddr);
  sockAddr.sin_family = AF_INET;
  sockAddr.sin_port = ntohs(12345);
  /*
   * INADDR_LOOPBACK = 127.0.0.1
   * INADDR_ANY = 0.0.0.0
   * inet_addr("10.0.0.1") IPv4
   * inet_pton(AF_INET, "10.0.0.1", &(sa.sin_addr))
   */
  sockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  if ( bind(masterSocket, (struct sockaddr *)&sockAddr, sizeof sockAddr) < 0)
    {
      perror ( "bind" );
      exit ( EXIT_FAILURE );
    }
  /*
   * SOMAXCONN = 128
   */
  if ( listen(masterSocket, SOMAXCONN) )
    {
      perror ( "listen" );
      exit ( EXIT_FAILURE );
    }
  int slaveSocket;
  /*
   * client address
   */
  struct sockaddr_in clientAddr;
  bzero(&clientAddr, sizeof clientAddr);
  unsigned int b = sizeof clientAddr;
  unsigned int counter = 0;
  while(slaveSocket = accept(masterSocket, (struct sockaddr *)&clientAddr, &b))
    {
      int one = 1;
      setsockopt(slaveSocket, SOL_SOCKET, SO_NOSIGPIPE, &one, sizeof one);
      
      char buffer[5]{ 0 };
      while (counter < 4)
	{
	  int res = recv(slaveSocket, buffer + counter, 1, 0);
	  if (res > 0)
	    {
	      counter += res;
	    }
	}
      if (counter == 4)
	{
	  counter = 0;
	}
      send(slaveSocket, buffer, 4, 0);
      std::cout << buffer << std::endl;
      shutdown(slaveSocket, SHUT_RDWR);
      close(slaveSocket);
    }
  /*
   * SHUT_RDWR read && write
   * SHUT_RD read
   * SHUT_WR write
   */
  shutdown(masterSocket, SHUT_RDWR);
  close(masterSocket);
}
