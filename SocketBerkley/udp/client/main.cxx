#include "main.hxx"
#include <sys/socket.h> // socket
#include <netinet/in.h> // IPPROTO_UDP
#include <strings.h> // bzero
#include <unistd.h> // close
#include <fcntl.h> // fcntl
#include <sys/ioctl.h> // ioctl

int main(int argc, char *arcv[])
{
  client();
  
  return EXIT_SUCCESS;
}

void client()
{
  /* -*- Domain -*-
   * AF_INET - IPv4
   * AF_INET6 - IPv6
   * AF_INET
   * -*- Type -*-
   * SOCK_STREAM - TCP
   * SOCK_DGRAM - UDP
   * IPPROTO_TCP
   * IPPROTO_UDP
   */
  int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (sock < 0)
    {
      perror ( "socket" );
      exit ( EXIT_FAILURE );
    }

  struct sockaddr_in sockAddr;
  bzero(&sockAddr, sizeof sockAddr);
  sockAddr.sin_family = AF_INET;
  sockAddr.sin_port = ntohs(12345);
  sockAddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

  int one = 1;
  setsockopt(sock, SOL_SOCKET, SO_NOSIGPIPE, &one, sizeof(one));
      
  char buffer[] = "PING";

  struct sockaddr_in from;
  socklen_t fromlen = sizeof from;
  sendto(sock, buffer, 4, 0, (struct sockaddr *)&sockAddr, sizeof sockAddr);
  recvfrom(sock, buffer, 4, 0, (struct sockaddr *)&from, &fromlen);
  
  std::cout << buffer << std::endl;
  
  shutdown(sock, SHUT_RDWR);
  close(sock);
}
