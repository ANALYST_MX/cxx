#include "main.hxx"
#include <sys/socket.h> // socket
#include <netinet/in.h> // IPPROTO_UDP
#include <strings.h> // bzero
#include <unistd.h> // close
#include <fcntl.h> // fcntl
#include <sys/ioctl.h> // ioctl

int main(int argc, char *arcv[])
{
  server();
  
  return EXIT_SUCCESS;
}

int set_nonblock(int fd)
{
  int flags;
#if defined(O_NONBLOCK)
  if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
    {
      flags = 0;
    }
  return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
  flags = 1;
  return ioctl(fd, FIOBIO, &flags);
#endif
}

void server()
{
  /* -*- Domain -*-
   * AF_INET - IPv4
   * AF_INET6 - IPv6
   * AF_INET
   * -*- Type -*-
   * SOCK_STREAM - TCP
   * SOCK_DGRAM - UDP
   * -*- Protocol -*-
   * IPPROTO_TCP
   * IPPROTO_UDP
   */
  int s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  
  int optval = 1;
  setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);
  int one = 1;
  setsockopt(s, SOL_SOCKET, SO_NOSIGPIPE, &one, sizeof one);
  /*
   * UNIX - socket:
   * struct sockaddr_un sa;
   * sa.sun_family = AF_UNIX;
   * strcpy(sa.sun_path, "/tmp/a.sock");
   */
  struct sockaddr_in sa;
  bzero(&sa, sizeof sa);
  sa.sin_family = AF_INET;
  sa.sin_port = ntohs(12345);
  /*
   * INADDR_LOOPBACK = 127.0.0.1
   * INADDR_ANY = 0.0.0.0
   * inet_addr("10.0.0.1") IPv4
   * inet_pton(AF_INET, "10.0.0.1", &(sa.sin_addr))
   */
  sa.sin_addr.s_addr = htonl(INADDR_ANY);
  bind(s, (struct sockaddr *)&sa, sizeof sa);
  struct sockaddr_in from;
  bzero(&from, sizeof from);
  while (true)
    {
      char buffer[5]{ 0 };
      socklen_t fromlen = sizeof from;
      recvfrom(s, buffer, 4, 0, (struct sockaddr *)&from, &fromlen);
      sendto(s, buffer, 4, 0, (struct sockaddr *)&from, sizeof from);
      std::cout << buffer << std::endl;
    }
  /*
   * SHUT_RDWR read && write
   * SHUT_RD read
   * SHUT_WR write
   */
  shutdown(s, SHUT_RDWR);
  close(s);
}
