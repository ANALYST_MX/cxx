#include "main.hxx"
#include <poll.h>

#define POLL_SIZE 2048

int main(int argc, char *argv[])
{
  echo_server();
  
  return EXIT_SUCCESS;
}

void echo_server()
{
  int masterSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

  int optval = 1;
  setsockopt(masterSocket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

  std::set<int> slaveSockets;
  struct sockaddr_in sockAddr;
  sockAddr.sin_family = AF_INET;
  sockAddr.sin_port = htons(12345);
  sockAddr.sin_addr.s_addr = htonl(INADDR_ANY);

  bind(masterSocket, (struct sockaddr *)&sockAddr, sizeof sockAddr);

  set_nonblock(masterSocket);
  listen(masterSocket, SOMAXCONN);
  
  struct pollfd fdSet[POLL_SIZE];
  fdSet[0].fd = masterSocket;
  fdSet[0].events = POLLIN; // READ
  while (true)
    {
      unsigned int index = 1;
      for (auto it = slaveSockets.begin(); it != slaveSockets.end(); ++it, ++index)
	{
	  fdSet[index].fd = *it;
	  fdSet[index].events = POLLIN;
	}
      unsigned int setSize = 1 + slaveSockets.size();
      /*
       * int poll(struct pollfd fds[], nfds_t nfds, int timeout);
       * If the value of timeout is -1, the poll blocks indefinitely
       */
      poll(fdSet, setSize, -1);
      for (unsigned int i = 0; i < setSize; ++i)
	{
	  if (fdSet[i].revents & POLLIN)
	    {
	      if (i != 0)
		{
		  static char buffer[1024];
		  int recvSize = recv(fdSet[i].fd, buffer, 1024, 0);
		  if (recvSize == 0 && errno != EAGAIN)
		    {
		      shutdown(fdSet[i].fd, SHUT_RDWR);
		      close(fdSet[i].fd);
		      slaveSockets.erase(fdSet[i].fd);
		    }
		  else if (recvSize > 0)
		    {
		      send(fdSet[i].fd, buffer, recvSize, 0);
		    }
		}
	      else
		{
		  int slaveSocket = accept(masterSocket, nullptr, nullptr);
		  int one = 1;
		  setsockopt(slaveSocket, SOL_SOCKET, SO_NOSIGPIPE, &one, sizeof one);
		  set_nonblock(slaveSocket);
		  slaveSockets.insert(slaveSocket);
		}
	    }
	}
    }
}

int set_nonblock(int fd)
{
  int flags;
#if defined(O_NONBLOCK)
  if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
    {
      flags = 0;
    }
  return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
  flags = 1;
  return ioctl(fd, FIOBIO, &flags);
#endif
}


