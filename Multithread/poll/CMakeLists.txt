cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)

project(poll CXX)

add_executable(poll main.cxx)
target_compile_features(poll PRIVATE cxx_range_for)
