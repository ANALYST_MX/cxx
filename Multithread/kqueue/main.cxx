#include "main.hxx"
#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>

#define POLL_SIZE 2048

int main(int argc, char *argv[])
{
  echo_server();
  
  return EXIT_SUCCESS;
}

void echo_server()
{
  int masterSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

  int optval = 1;
  setsockopt(masterSocket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

  struct sockaddr_in sockAddr;
  sockAddr.sin_family = AF_INET;
  sockAddr.sin_port = htons(12345);
  sockAddr.sin_addr.s_addr = htonl(INADDR_ANY);

  bind(masterSocket, (struct sockaddr *)&sockAddr, sizeof sockAddr);

  set_nonblock(masterSocket);
  listen(masterSocket, SOMAXCONN);

  /*
   * int kqueue(void);
   */
  int kq = kqueue();
  struct kevent kqEvent;
  bzero(&kqEvent, sizeof kqEvent);
  /*
   * EV_SET(&kev, ident, filter, flags, fflags, data, udata);
   */
  EV_SET(&kqEvent, masterSocket, EVFILT_READ, EV_ADD, 0, 0, 0);
  /*
   * int kevent(int kq, const struct kevent *changelist, int nchanges, struct kevent *eventlist, int nevents,
   *            const struct timespec *timeout);
   */
  kevent(kq, &kqEvent, 1, nullptr, 0, nullptr);
  while (true)
    {
      bzero(&kqEvent, sizeof kqEvent);
      kevent(kq, nullptr, 0, &kqEvent, 1, nullptr);
      if (kqEvent.filter == EVFILT_READ)
	{
	  if (kqEvent.ident != masterSocket)
	    {
	      static char buffer[1024];
	      int recvSize = recv(kqEvent.ident, buffer, 1024, 0);
	      if (recvSize == 0 && errno != EAGAIN)
		{
		  shutdown(kqEvent.ident, SHUT_RDWR);
		  close(kqEvent.ident);
		}
	      else if (recvSize > 0)
		{
		  send(kqEvent.ident, buffer, recvSize, 0);
		}
	    }
	  else
	    {
	      int slaveSocket = accept(masterSocket, nullptr, nullptr);
	      int one = 1;
	      setsockopt(slaveSocket, SOL_SOCKET, SO_NOSIGPIPE, &one, sizeof one);
	      set_nonblock(slaveSocket);
	      bzero(&kqEvent, sizeof kqEvent);
	      EV_SET(&kqEvent, slaveSocket, EVFILT_READ, EV_ADD, 0, 0, 0);
	      kevent(kq, &kqEvent, 1, nullptr, 0, nullptr);
	    }
	}
    }
}

int set_nonblock(int fd)
{
  int flags;
#if defined(O_NONBLOCK)
  if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
    {
      flags = 0;
    }
  return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
  flags = 1;
  return ioctl(fd, FIOBIO, &flags);
#endif
}


