#include "main.hxx"
#include <sys/select.h>

int main(int argc, char *argv[])
{
  echo_server();
  
  return EXIT_SUCCESS;
}

void echo_server()
{
  int masterSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  int optval = 1;
  setsockopt(masterSocket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);
  std::set<int> slaveSockets;
  struct sockaddr_in sockAddr;
  sockAddr.sin_family = AF_INET;
  sockAddr.sin_port = htons(12345);
  sockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  bind(masterSocket, (struct sockaddr *)&sockAddr, sizeof sockAddr);
  set_nonblock(masterSocket);
  listen(masterSocket, SOMAXCONN);

  while (true)
    {
      fd_set fdSet; // 1024 bit [0|1], [STDIN,STDOUT,STDERR,...]
      FD_ZERO(&fdSet);
      FD_SET(masterSocket, &fdSet); // [STDIN,STDOUT,STDERR,masterSocket,...]

      for (auto it = slaveSockets.begin(); it != slaveSockets.end(); ++it)
	{
	  FD_SET(*it, &fdSet);
	}
      int max = std::max(masterSocket, *std::max_element(slaveSockets.begin(),
							 slaveSockets.end()));
      /*
       * int select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds, struct timeval *timeout);
       * accept - readfds
       */
      select(max + 1, &fdSet, nullptr, nullptr, nullptr);

      for (auto it = slaveSockets.begin(); it != slaveSockets.end(); ++it)
	{
	  if (FD_ISSET(*it, &fdSet))
	    {
	      static char buffer[1024];
	      int recvSize = recv(*it, buffer, 1024, 0);
	      if (recvSize == 0 && errno != EAGAIN)
		{
		  shutdown(*it, SHUT_RDWR);
		  close(*it);
		  slaveSockets.erase(*it);
		}
	      else if (recvSize > 0)
		{
		  send(*it, buffer, recvSize, 0);
		}
	    }
	}
      if (FD_ISSET(masterSocket, &fdSet))
	{
	  int slaveSocket = accept(masterSocket, nullptr, nullptr);
	  int one = 1;
	  setsockopt(slaveSocket, SOL_SOCKET, SO_NOSIGPIPE, &one, sizeof one);
	  set_nonblock(slaveSocket);
	  slaveSockets.insert(slaveSocket);
	}
    }
}

int set_nonblock(int fd)
{
  int flags;
#if defined(O_NONBLOCK)
  if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
    {
      flags = 0;
    }
  return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
  flags = 1;
  return ioctl(fd, FIOBIO, &flags);
#endif
}


